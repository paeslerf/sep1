package LoveLetter;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.ArrayList;

/**
 * Handles filling the console with relevant information
 */
public class View {
    private final ViewModel _viewModel;
    private final String[] _screen;

    // Ansi style templates
    private final String SIDE = "\033[43m   \033[0m";
    private final String MID2 = "\033[43m  \033[0m";
    private final String MID1 = "\033[43m \033[0m";
    private final String RESET = "\033[0m";
    private final String BACKGROUND_YELLOW = "\033[43m";
    private final String TEXT_GREY_ITALIC = "\033[0;90;3m";
    private final String HEART = "\033[41m  \033[0m";
    private final String PLAYER_OUT = "\033[0;90;9m";
    private final String PLAYER1 = "\033[0;33m";
    private final String PLAYER1_PROTECTED = "\033[0;33;4m";
    private final String PLAYER1_PLAYED_CARDS = "\033[0;33;3m";
    private final String PLAYER2 = "\033[0;31m";
    private final String PLAYER2_PROTECTED = "\033[0;31;4m";
    private final String PLAYER2_PLAYED_CARDS = "\033[0;31;3m";
    private final String PLAYER3 = "\033[0;35m";
    private final String PLAYER3_PROTECTED = "\033[0;35;4m";
    private final String PLAYER3_PLAYED_CARDS = "\033[0;35;3m";
    private final String PLAYER4 = "\033[0;32m";
    private final String PLAYER4_PROTECTED = "\033[0;32;4m";
    private final String PLAYER4_PLAYED_CARDS = "\033[0;32;3m";
    private final String CARD_REGULAR = "\033[47;90m";
    private final String CARD_SELECTED = "\033[107;90;1m";
    private final String TEXT_RED = "\033[0;31m";
    private final String TEXT_YELLOW = "\033[0;33m";
    private final String TEXT_YELLOW_ITALIC = "\033[0;33;3m";
    private final String TEXT_GREY = "\033[0;90m";
    private final String BACKGROUND_YELLOW_TEXT_GREY = "\033[43;90m";
    private final String WIN_SCREEN_HEART = "\33[101;97;1;3m";

    /**
     * Create the view for the game and connect it to the view model
     * @param viewModel viewModel in between view and game
     */
    public View(ViewModel viewModel) {
        this._viewModel = viewModel;
        _screen = new String[28];
    }

    /**
     * Routine for input and screen refreshing. Starts interactive part of program
     */
    public void display() {
        while (_viewModel.isStillRunning()) {
            fillScreen();
            displayScreen();
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();
            input = input.trim();

            if (_viewModel.isValidInput(input))
                _viewModel.insertInput(input);
            else
                _viewModel.updateMessage();
        }
        clearConsole();
    }

    /**
     * Refreshes the screen by clearing console and printing current screen content
     */
    private void displayScreen() {
        clearConsole();
        System.out.println(RESET);

        for (int i = 0; i < 28; i++)
            System.out.println(_screen[i]);
    }

    /**
     * Fills screen accordingly to current ActionState
     */
    private void fillScreen() {
        for (int i = 0; i < 28; i++)
            _screen[i] = "";

        Action phase = _viewModel.getActionState();

        switch (phase) {
            case drawCard, newRound -> fillScreenWaiting();
            case pickCard,pickValueNot1,pickOtherUnprotectedPlayerInPlay,
                    pickUnprotectedPlayerInPlay, endTurn -> fillScreenGame();
            case help -> fillScreenHelp();
            case enterPlayerName, enterPlayerNumber -> fillScreenSetup();
            case start -> fillScreenWelcome();
            case gameEnded -> fillScreenWin();
        }
    }

    /**
     * Fills screen during waiting period between two turns
     */
    private void fillScreenWaiting() {
        int roundNumber = _viewModel.getRoundNumber() + 1;
        String message = _viewModel.getMessage();
        String topBottomLine = BACKGROUND_YELLOW +" ".repeat(80) + RESET;
        String midLine = BACKGROUND_YELLOW + "   " + RESET + " ".repeat(74) + BACKGROUND_YELLOW + "   " + RESET;
        _screen[0] = topBottomLine;

        for(int i = 1; i < 9; i++)
            _screen[i] = midLine;

        String round = "ROUND " + roundNumber;
        _screen[9] = BACKGROUND_YELLOW + "   " + RESET + TEXT_RED + padStringBothSides(round, 74) + RESET
                + BACKGROUND_YELLOW + "   " + RESET;
        _screen[10] = midLine;
        _screen[11] = midLine;
        String getReadyPlayerName = "Get ready " + _viewModel.getCurrentPlayerName() + "!";
        _screen[12] = BACKGROUND_YELLOW + "   " + RESET + TEXT_YELLOW
                + padStringBothSides(getReadyPlayerName, 74) + RESET + BACKGROUND_YELLOW + "   " + RESET;
        _screen[13] = midLine;
        _screen[14] = BACKGROUND_YELLOW + "   " + RESET + padStringBothSides(message, 74) + RESET
                + BACKGROUND_YELLOW + "   " + RESET;

        for (int i = 15; i < 27; i++)
            _screen[i] = midLine;

        _screen[27] = BACKGROUND_YELLOW_TEXT_GREY
                + padStringBothSides("<\\startTurn> or <\\showHand> or <\\showPlayers> " +
                "to begin your turn", 80) + RESET;
    }

    /**
     * Picks correct method to fill screen depending on amount of players
     */
    private void fillScreenGame() {
        int numberOfPlayers = _viewModel.getNumberOfPlayers();

        switch (numberOfPlayers) {
            case 2 -> fillScreenGame2Players();
            case 3 -> fillScreenGame3Players();
            case 4 -> fillScreenGame4Players();
        }
    }

    /**
     * Fills screen during 2 player game
     */
    private void fillScreenGame2Players() {
        Hashtable<String,String> player2 = _viewModel.getPlayerInfo(1);
        Hashtable<String,String> player2PlayedCards = getPlayedCardsHashtable(player2);

        boolean showScore = _viewModel.getShowScore();
        int player2Hearts = Integer.parseInt(player2.get("hearts"));
        String player2Color;

        if(player2.get("isProtected").equals("true"))
            player2Color = PLAYER2_PROTECTED;
        else
            player2Color = PLAYER2;

        if(player2.get("inPlay").equals("false"))
            player2Color = PLAYER_OUT;

        String midLine = BACKGROUND_YELLOW + "   " + RESET + " ".repeat(74) + BACKGROUND_YELLOW + "   " + RESET;
        _screen[0] = BACKGROUND_YELLOW_TEXT_GREY
                + padStringBothSides(_viewModel.getActivePlayerCardTrades(), 80) + RESET;
        _screen[1] = midLine;

        if (showScore)
            _screen[2] = SIDE + CreatePlayerHeartLine(player2Hearts, 74) + SIDE;
        else
            _screen[2] = SIDE + TEXT_GREY_ITALIC
                    + padStringBothSides("hidden score",74) + RESET + SIDE;

        _screen[3] = BACKGROUND_YELLOW + "   " + RESET + player2Color
                + padStringBothSides(player2.get("name"), 74) + RESET + BACKGROUND_YELLOW + "   " + RESET;
        _screen[4] = BACKGROUND_YELLOW + "   " + RESET + PLAYER2
                + padStringBothSides("Played Cards", 74) + RESET + BACKGROUND_YELLOW + "   " + RESET;
        int line = 5;

        for(int i = 1; i <= player2PlayedCards.size(); i++) {
            String card = i + ": " + player2PlayedCards.get(String.valueOf(i));
            _screen[line] = BACKGROUND_YELLOW + "   " + RESET + TEXT_GREY + padStringBothSides(card, 74)
                    + RESET + BACKGROUND_YELLOW + "   " + RESET;
            line++;
        }

        while (line < 13) {
            _screen[line] = midLine;
            line++;
        }

        fillActivePlayerPartOfScreenDuringGame();
    }

    /**
     * Fills screen during 3 player game
     */
    private void fillScreenGame3Players() {
        Hashtable<String,String> player2 = _viewModel.getPlayerInfo(1);
        Hashtable<String,String> player3 = _viewModel.getPlayerInfo(2);
        Hashtable<String,String> player2PlayedCards = getPlayedCardsHashtable(player2);
        Hashtable<String,String> player3PlayedCards = getPlayedCardsHashtable(player3);

        boolean showScore = _viewModel.getShowScore();
        int player2Hearts = Integer.parseInt(player2.get("hearts"));
        int player3Hearts = Integer.parseInt(player3.get("hearts"));

        String player2Color;
        String player3Color;

        if(player2.get("isProtected").equals("true"))
            player2Color = PLAYER2_PROTECTED;
        else
            player2Color = PLAYER2;

        if(player2.get("inPlay").equals("false"))
            player2Color = PLAYER_OUT;

        if(player3.get("isProtected").equals("true"))
            player3Color = PLAYER3_PROTECTED;
        else
            player3Color = PLAYER3;

        if(player3.get("inPlay").equals("false"))
            player3Color = PLAYER_OUT;

        String midLine = BACKGROUND_YELLOW + "   " + RESET + " ".repeat(36) + MID2 + " ".repeat(36)
                + BACKGROUND_YELLOW + "   " + RESET;
        _screen[0] = BACKGROUND_YELLOW_TEXT_GREY
                + padStringBothSides(_viewModel.getActivePlayerCardTrades(), 80) + RESET;
        _screen[1] = midLine;

        if (showScore)
            _screen[2] = SIDE
                    + CreatePlayerHeartLine(player2Hearts, 36) + MID2
                    + CreatePlayerHeartLine(player3Hearts, 36) + SIDE;
        else
            _screen[2] = SIDE
                    + TEXT_GREY_ITALIC + padStringBothSides("hidden score", 36) + RESET + MID2
                    + TEXT_GREY_ITALIC + padStringBothSides("hidden score", 36) + RESET + SIDE;

        _screen[3] = SIDE
                + player2Color + padStringBothSides(player2.get("name"), 36) + RESET + MID2
                + player3Color + padStringBothSides(player3.get("name"), 36) + RESET + SIDE;
        _screen[4] = SIDE
                + PLAYER2_PLAYED_CARDS + padStringBothSides("Played Cards", 36) + RESET + MID2
                + PLAYER3_PLAYED_CARDS + padStringBothSides("Played Cards", 36) + RESET + SIDE;
        int line = 5;

        for(int i = 1; i <= player2PlayedCards.size(); i++) {
            String card = i + ": " + player2PlayedCards.get(String.valueOf(i));
            _screen[line] = SIDE + TEXT_GREY + padStringBothSides(card, 36) + RESET + MID2;
            line++;
        }

        while (line < 13) {
            _screen[line] = SIDE + " ".repeat(36) + MID2;
            line++;
        }

        line = 5;

        for(int i = 1; i <= player3PlayedCards.size(); i++) {
            String card = i + ": " + player3PlayedCards.get(String.valueOf(i));
            _screen[line] += TEXT_GREY + padStringBothSides(card, 36) + RESET + SIDE;
            line++;
        }

        while (line < 13) {
            _screen[line] += " ".repeat(36) + SIDE;
            line++;
        }

        fillActivePlayerPartOfScreenDuringGame();
    }

    /**
     * Fills screen during 4 player game
     */
    private void fillScreenGame4Players() {
        Hashtable<String,String> player2 = _viewModel.getPlayerInfo(1);
        Hashtable<String,String> player3 = _viewModel.getPlayerInfo(2);
        Hashtable<String,String> player4 = _viewModel.getPlayerInfo(3);
        Hashtable<String,String> player2PlayedCards = getPlayedCardsHashtable(player2);
        Hashtable<String,String> player3PlayedCards = getPlayedCardsHashtable(player3);
        Hashtable<String,String> player4PlayedCards = getPlayedCardsHashtable(player4);

        boolean showScore = _viewModel.getShowScore();
        int player2Hearts = Integer.parseInt(player2.get("hearts"));
        int player3Hearts = Integer.parseInt(player3.get("hearts"));
        int player4Hearts = Integer.parseInt(player4.get("hearts"));
        String player2Color;
        String player3Color;
        String player4Color;

        if(player2.get("isProtected").equals("true"))
            player2Color = PLAYER2_PROTECTED;
        else
            player2Color = PLAYER2;

        if(player2.get("inPlay").equals("false"))
            player2Color = PLAYER_OUT;

        if(player3.get("isProtected").equals("true"))
            player3Color = PLAYER3_PROTECTED;
        else
            player3Color = PLAYER3;

        if(player3.get("inPlay").equals("false"))
            player3Color = PLAYER_OUT;

        if(player4.get("isProtected").equals("true"))
            player4Color = PLAYER4_PROTECTED;
        else
            player4Color = PLAYER4;

        if(player4.get("inPlay").equals("false"))
            player4Color = PLAYER_OUT;

        String midLine = SIDE + " ".repeat(24) + MID1 + " ".repeat(24)
                + MID1 + " ".repeat(24) + SIDE;
        _screen[0] = BACKGROUND_YELLOW_TEXT_GREY
                + padStringBothSides(_viewModel.getActivePlayerCardTrades(), 80) + RESET;
        _screen[1] = midLine;

        if (showScore)
            _screen[2] = SIDE
                    + CreatePlayerHeartLine(player2Hearts, 24) + MID1
                    + CreatePlayerHeartLine(player3Hearts, 24) + MID1
                    + CreatePlayerHeartLine(player4Hearts, 24) + SIDE;
        else
            _screen[2] = SIDE
                    + TEXT_GREY_ITALIC + padStringBothSides("hidden score", 24) + RESET + MID1
                    + TEXT_GREY_ITALIC + padStringBothSides("hidden score", 24) + RESET + MID1
                    + TEXT_GREY_ITALIC + padStringBothSides("hidden score", 24) + RESET + SIDE;

        _screen[3] = SIDE
                + player2Color + padStringBothSides(player2.get("name"), 24) + RESET + MID1
                + player3Color + padStringBothSides(player3.get("name"), 24) + RESET + MID1
                + player4Color + padStringBothSides(player4.get("name"), 24) + RESET + SIDE;
        _screen[4] = SIDE
                + PLAYER2_PLAYED_CARDS + padStringBothSides("Played Cards", 24) + RESET + MID1
                + PLAYER3_PLAYED_CARDS + padStringBothSides("Played Cards", 24) + RESET + MID1
                + PLAYER4_PLAYED_CARDS + padStringBothSides("Played Cards", 24) + RESET + SIDE;
        int line = 5;

        for(int i = 1; i <= player2PlayedCards.size(); i++) {
            String card = i + ": " + player2PlayedCards.get(String.valueOf(i));
            _screen[line] = SIDE + TEXT_GREY + padStringBothSides(card, 24) + RESET + MID1;
            line++;
        }

        while (line < 13) {
            _screen[line] = SIDE + " ".repeat(24) + MID1;
            line++;
        }

        line = 5;

        for(int i = 1; i <= player3PlayedCards.size(); i++) {
            String card = i + ": " + player3PlayedCards.get(String.valueOf(i));
            _screen[line] += TEXT_GREY + padStringBothSides(card, 24) + RESET + MID1;
            line++;
        }

        while (line < 13) {
            _screen[line] += " ".repeat(24) + MID1;
            line++;
        }

        line = 5;

        for(int i = 1; i <= player4PlayedCards.size(); i++) {
            String card = i + ": " + player4PlayedCards.get(String.valueOf(i));
            _screen[line] += TEXT_GREY + padStringBothSides(card, 24) + RESET + SIDE;
            line++;
        }

        while (line < 13) {
            _screen[line] += " ".repeat(24) + SIDE;
            line++;
        }
        fillActivePlayerPartOfScreenDuringGame();
    }

    /**
     * Fills bottom part of screen during gameplay containing the active player info
     */
    private void fillActivePlayerPartOfScreenDuringGame() {
        Hashtable<String,String> playerActive = _viewModel.getPlayerInfo(0);
        Hashtable<String,String> gameInfo = _viewModel.getGameInfo();
        Hashtable<String,String> playerActivePlayedCards = getPlayedCardsHashtable(playerActive);
        int playerActiveHearts = Integer.parseInt(playerActive.get("hearts"));
        int numberOfPlayers = _viewModel.getNumberOfPlayers();
        boolean showScore = _viewModel.getShowScore();

        String midLine = SIDE + " ".repeat(74) + SIDE;
        String playerActiveColor;
        String infoText;
        int line = 19;

        if (gameInfo.get("message") == null)
            _screen[13] = midLine;
        else
            _screen[13] = BACKGROUND_YELLOW + "   " + RESET
                    + padStringBothSides(gameInfo.get("message"), 74) + BACKGROUND_YELLOW + "   " + RESET;

        if (gameInfo.containsKey("sideCard"))
            infoText = "Cards left: " + gameInfo.get("remainingDeckSize") + "+1  ";
        else
            infoText = "Cards left: " + gameInfo.get("remainingDeckSize") + "    ";

        if (numberOfPlayers == 2) {
            infoText += "Open Cards: " + gameInfo.get("openCard1") + ", " + gameInfo.get("openCard2")
                    + " and " + gameInfo.get("openCard3");
        }

        _screen[14] = BACKGROUND_YELLOW + "   " + RESET + padStringBothSides(infoText, 74)
                + BACKGROUND_YELLOW + "   " + RESET;
        _screen[15] = midLine;

        if (showScore)
            _screen[16] = SIDE +  CreatePlayerHeartLine(playerActiveHearts, 74) + SIDE;
        else
            _screen[16] = SIDE + TEXT_GREY_ITALIC + padStringBothSides("hidden score", 74)
                    + RESET + SIDE;

        if(playerActive.get("isProtected").equals("true"))
            playerActiveColor = PLAYER1_PROTECTED;
        else
            playerActiveColor = PLAYER1;

        if(playerActive.get("inPlay").equals("false"))
            playerActiveColor = PLAYER_OUT;

        _screen[17] = BACKGROUND_YELLOW + "   " + RESET + playerActiveColor
                + padStringBothSides(playerActive.get("name"), 74) + RESET + BACKGROUND_YELLOW + "   " + RESET;
        _screen[18] = BACKGROUND_YELLOW + "   " + RESET + PLAYER1_PLAYED_CARDS
                + padStringBothSides("Played Cards", 74) + RESET + BACKGROUND_YELLOW + "   " + RESET;

        for(int i = 1; i <= playerActivePlayedCards.size(); i++) {
            String card = i + ": " + playerActivePlayedCards.get(String.valueOf(i));
            _screen[line] = BACKGROUND_YELLOW + "   " + RESET + TEXT_GREY + padStringBothSides(card, 74)
                    + RESET + BACKGROUND_YELLOW + "   " + RESET;
            line++;
        }

        while (line < 26) {
            _screen[line] = midLine;
            line++;
        }

        String leftCardColor = CARD_REGULAR;
        String leftCard = gameInfo.get("leftCard_Value") + " " + gameInfo.get("leftCard_Role");
        String rightCardColor = CARD_REGULAR;
        String rightCard = gameInfo.get("rightCard_Value") + " " + gameInfo.get("rightCard_Role");

        if (_viewModel.getSideOfPickedCard().equals("left"))
            leftCardColor = CARD_SELECTED;

        if (_viewModel.getSideOfPickedCard().equals("right"))
            rightCardColor =CARD_SELECTED;

        _screen[26] = BACKGROUND_YELLOW + "   " + RESET + leftCardColor
                + padStringLeft(padStringRight(padStringLeft(leftCard,20),23),37)
                + RESET + rightCardColor
                + padStringRight(padStringLeft(padStringRight(rightCard,20),23),37)
                + RESET + BACKGROUND_YELLOW + "   " + RESET;
        fillScreenGameLine27TooltipsDuringGame();
    }

    /**
     * Adds last line to screen during gameplay containing input options
     */
    private void fillScreenGameLine27TooltipsDuringGame() {
        Action nextAction = _viewModel.getActionState();
        _screen[27] = BACKGROUND_YELLOW + " ".repeat(80) + RESET;
        switch (nextAction) {
            case pickCard ->
                    _screen[27] = BACKGROUND_YELLOW_TEXT_GREY + padStringBothSides("<\\left> or <\\right> to "
                            + "select card or <\\playCard> for random play", 80) + RESET;
            case pickValueNot1 ->
                    _screen[27] = BACKGROUND_YELLOW_TEXT_GREY + padStringBothSides("<2>, <3>,..., <8> to select "
                            + "a card via value.", 80) + RESET;
            case pickUnprotectedPlayerInPlay -> {
                ArrayList<String> playersUnprotectedInPlay = _viewModel.getPickablePlayerNames();
                String optionsUnprotectedInPlay = "";
                if (playersUnprotectedInPlay.size() > 0) {
                    optionsUnprotectedInPlay = "<" + playersUnprotectedInPlay.get(0) + ">";

                    for (int i = 1; i < playersUnprotectedInPlay.size(); i++)
                        optionsUnprotectedInPlay += " or <" + playersUnprotectedInPlay.get(i) + ">";

                    optionsUnprotectedInPlay += " pick a player by their name";
                }
                if (optionsUnprotectedInPlay.length() > 80)
                    optionsUnprotectedInPlay = "<name of player> pick an unprotected player by their name";
                _screen[27] = BACKGROUND_YELLOW_TEXT_GREY + padStringBothSides(optionsUnprotectedInPlay, 80)
                        + RESET;
            }
            case pickOtherUnprotectedPlayerInPlay -> {
                ArrayList<String> players = _viewModel.getPickablePlayerNames();
                String options = "";
                if (players.size() > 0) {
                    options = "<" + players.get(0) + ">";

                    for (int i = 1; i < players.size(); i++)
                        options += " or <" + players.get(i) + ">";

                    options += " pick a player by their name";
                }
                if (options.length() > 80)
                    options = "<name of player> pick another unprotected player by their name";
                _screen[27] = BACKGROUND_YELLOW_TEXT_GREY + padStringBothSides(options, 80) + RESET;
            }
            case endTurn -> _screen[27] = BACKGROUND_YELLOW_TEXT_GREY
                    + padStringBothSides("<\\endTurn> to end your turn", 80) + RESET;
        }
    }


    /**
     * Fills the screen during game Setup (enter amount of players and their names)
     */
    private void fillScreenSetup() {
        String topBottomLine = BACKGROUND_YELLOW + " ".repeat(80) + RESET;
        String midLine = BACKGROUND_YELLOW + "   " + RESET + " ".repeat(74) + BACKGROUND_YELLOW + "   " + RESET;
        String messageLine = BACKGROUND_YELLOW + "   " + RESET + TEXT_RED
                + padStringBothSides(_viewModel.getMessage(),74) + RESET + BACKGROUND_YELLOW + "   " + RESET;
        String[] playerLines = new String[_viewModel.getNumberOfCreatedPlayers()];

        for(int i = 0; i < playerLines.length; i++) {
            playerLines[i] =  BACKGROUND_YELLOW + "   " + RESET + TEXT_YELLOW
                    + padStringBothSides(_viewModel.getPlayerName(i),74) + RESET + BACKGROUND_YELLOW + "   "
                    + RESET;
        }

        int line = 0;

        while (line < 27 ) {
            if (line == 0) {
                _screen[line] = topBottomLine;
                line++;
            }

            if(line == 9) {
                _screen[line] = messageLine;
                line++;
            }

            if(line == 11) {
                for(String playerLine : playerLines) {
                    _screen[line] = playerLine;
                    line++;
                }
            }

            if (line < 27) {
                _screen[line] = midLine;
                line++;
            }
        }
        Action nextAction = _viewModel.getActionState();
        _screen[27] = topBottomLine;

        if (nextAction == Action.enterPlayerNumber)
            _screen[27] = BACKGROUND_YELLOW_TEXT_GREY
                    + padStringBothSides("<2>, <3> or <4> to enter the amount of players",80) + RESET;

        if (nextAction == Action.enterPlayerName)
            _screen[27] = BACKGROUND_YELLOW_TEXT_GREY
                    + padStringBothSides("<name> for player. Only letters and max length is 20", 80)
                    + RESET;
    }

    /**
     * Fills the screen when the game is first started
     */
    private void fillScreenWelcome() {
        String topLine = BACKGROUND_YELLOW_TEXT_GREY
                + padStringBothSides("You can always ask for help with <\\help>", 80) + RESET;
        String midLine = BACKGROUND_YELLOW + "   " + RESET + " ".repeat(74) + BACKGROUND_YELLOW + "   " + RESET;
        String bottomLine = BACKGROUND_YELLOW_TEXT_GREY
                + padStringBothSides("Start a game by entering <\\start>", 80)  + RESET;

        String titleLine = BACKGROUND_YELLOW + "   " + RESET + " ".repeat(31) + TEXT_RED + "LOVE LETTER" + RESET
                + " ".repeat(32) + BACKGROUND_YELLOW + "   " + RESET;

        String byLine = BACKGROUND_YELLOW + "   " + RESET + " ".repeat(36) + TEXT_GREY + "by" + RESET
                + " ".repeat(36) + BACKGROUND_YELLOW + "   " + RESET;

        String nameLine = BACKGROUND_YELLOW + "   " + RESET + " ".repeat(30) + TEXT_GREY + "Felix  Paesler"
                + RESET + " ".repeat(30) + BACKGROUND_YELLOW + "   " + RESET;
        _screen[0] = topLine;
        _screen[27] = bottomLine;

        for (int i = 1; i < 27 ; i++)
            _screen[i] = midLine;

        _screen[9] = titleLine;
        _screen[11] = byLine;
        _screen[13] = nameLine;
    }

    /**
     * Fills the screen when a player has won the game
     */
    private void fillScreenWin() {
        _screen[0] = BACKGROUND_YELLOW + " ".repeat(80) + RESET;
        _screen[1] = SIDE + " ".repeat(74) + SIDE;
        _screen[2] = SIDE + TEXT_YELLOW_ITALIC + padStringBothSides("Congratulations!", 74)
                + SIDE;
        _screen[3] = SIDE + " ".repeat(74) + SIDE;
        _screen[4] = createLineHeartWinScreen(12, 8);
        _screen[5] = createLineHeartWinScreen(16, 4);
        _screen[6] = createLineHeartWinScreen(19,2);
        _screen[7] = createLineHeartWinScreen(42);
        _screen[8] = createLineHeartWinScreen(44);
        _screen[9] = createLineHeartWinScree(44, "Annette");
        _screen[10] = createLineHeartWinScreen(42);
        _screen[11] = createLineHeartWinScree(40, "&");
        _screen[12] = createLineHeartWinScreen(36);
        _screen[13] = createLineHeartWinScree(32, _viewModel.getWinnerName());
        _screen[14] = createLineHeartWinScreen(28);
        _screen[15] = createLineHeartWinScreen(24);
        _screen[16] = createLineHeartWinScreen(20);
        _screen[17] = createLineHeartWinScreen(16);
        _screen[18] = createLineHeartWinScreen(12);
        _screen[19] = createLineHeartWinScreen( 8);
        _screen[20] = createLineHeartWinScreen( 4);
        _screen[21] = createLineHeartWinScreen( 2);
        _screen[22] = _screen[1];
        _screen[23] = _screen[1];
        _screen[24] = SIDE + TEXT_YELLOW_ITALIC + padStringBothSides(_viewModel.getMessage(), 74)
                + RESET + SIDE;
        _screen[25] = _screen[1];
        _screen[26] = SIDE + TEXT_GREY +padStringBothSides("I hope you liked the game!", 74)
                + RESET + SIDE;
        _screen[27] = BACKGROUND_YELLOW_TEXT_GREY
                + padStringBothSides("<\\newGame> to start a new game <\\exit> to close game", 80)
                + RESET;
    }


    /**
     * Creates a line used in WinScreen heart display
     * @param red Size of red area
     * @return String that is supposed to be displayed in terminal
     */
    private String createLineHeartWinScreen(int red) {

        return SIDE + " ".repeat((74 - red) / 2)
                + WIN_SCREEN_HEART + " ".repeat(red) + RESET
                + " ".repeat((74 - red) / 2) + SIDE;
    }

    /**
     * Creates a line used in WinScreen heart display
     * @param red Size of red area on each side
     * @param inBetween Size of empty are between red background
     * @return String that is supposed to be displayed in terminal
     */
    private String createLineHeartWinScreen(int red, int inBetween) {

        return SIDE + " ".repeat(37 - red - (inBetween/2))
                + WIN_SCREEN_HEART + " ".repeat(red) + RESET
                + " ".repeat(inBetween)
                + WIN_SCREEN_HEART + " ".repeat(red) + RESET
                + " ".repeat(37 - red - (inBetween/2)) + SIDE;
    }

    /**
     * Creates a line used in WinScreen heart display with a string inside
     * @param red Size of red area
     * @param name String to display in it
     * @return String that is supposed to be displayed in terminal
     */
    private String createLineHeartWinScree(int red, String name) {

        return SIDE + " ".repeat((74 - red) / 2)
                + WIN_SCREEN_HEART + padStringBothSides(name, red) + RESET
                + " ".repeat((74 - red) / 2) + SIDE;
    }

    /**
     * Fills the screen when the help menu is opened
     */
    private void fillScreenHelp()
    {
        ArrayList<String[]> helpList = _viewModel.GetHelpOptions();
        int maxLengthParameter = 0;
        int maxLengthText = 0;
        for (String[] help: helpList) {
            if (maxLengthParameter < help[0].length())
                maxLengthParameter = help[0].length();
            if (maxLengthText < help[1].length())
                maxLengthText = help[1].length();
        }

        if (maxLengthText + maxLengthParameter > 71)
            throw new IndexOutOfBoundsException();

        int freePadding = 71 - maxLengthParameter - maxLengthText;
        int centerPadding = maxLengthParameter + 1;

        while (centerPadding <= maxLengthParameter + 3 && freePadding > 0) {
            centerPadding++;
            freePadding--;
        }

        freePadding = 74 - centerPadding - maxLengthText;
        int leftPadding = freePadding / 2;
        int rightPadding = freePadding - leftPadding;
        leftPadding += centerPadding;
        rightPadding += maxLengthText;

        for (String[] help : helpList) {
            help[0] = padStringRight(help[0], centerPadding);
            help[0] = padStringLeft(help[0], leftPadding);
            help[1] = padStringRight(help[1], rightPadding);
        }

        String topBottomLine = BACKGROUND_YELLOW + " ".repeat(80) + RESET;
        String midLine = BACKGROUND_YELLOW + "   " + RESET + " ".repeat(74) + BACKGROUND_YELLOW + "   " + RESET;
        String[] helpLines = new String[helpList.size()];

        for (int i = 0; i < helpList.size(); i++) {
            String[] help = helpList.get(i);
            helpLines[i] = BACKGROUND_YELLOW + "   " + RESET + TEXT_YELLOW + help[0] + RESET + TEXT_YELLOW_ITALIC
                    + help[1] + RESET + BACKGROUND_YELLOW + "   " + RESET;
        }

        String titleLine = BACKGROUND_YELLOW + "   " + RESET + TEXT_RED + " ".repeat(26)
                + "Library Of Dead Poets" + " ".repeat(27) + RESET + BACKGROUND_YELLOW + "   " + RESET;
        int line = 0;
        int nextHelpLine = 0;

        while (line < 27) {
            if (line == 0) {
                _screen[line] = topBottomLine;
                line++;
            }

            if (line == 4) {
                _screen[line] = titleLine;
                line++;
            }

            if (line > 6 && nextHelpLine < helpLines.length) {
                _screen[line] = helpLines[nextHelpLine];
                nextHelpLine++;
                line++;
            }

            if (line < 27) {
                _screen[line] = midLine;
                line++;
            }
        }

        _screen[27] = BACKGROUND_YELLOW_TEXT_GREY
                + padStringBothSides("<\\help> to leave library or use the shown commands directly", 80)
                + RESET;
    }

    /**
     * This clears the console, but I don't have access to non Windows machines so can't test it there.
     */
    private static void clearConsole() {
        try {
            if (System.getProperty("os.name").contains("Windows"))
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            else
                Runtime.getRuntime().exec("clear");
        }
        catch (IOException | InterruptedException ex) {System.out.println(("Clearing the console went wrong."));}
    }

    /**
     * Will pad a given string on both ends with spaces equally.
     * @param input String to center
     * @param totalLength How long you want the string to be
     * @return padded String or original input if totalLength < input length
     */
    private static String padStringBothSides(String input, int totalLength) {
        int padSize = totalLength - input.length();
        int rightPadSize = padSize / 2;
        int leftPadSize = padSize - rightPadSize;

        if (padSize <= 0)
            return input;

        return " ".repeat(leftPadSize) + input + " ".repeat(rightPadSize);
    }

    /**
     * Will pad a string on the left side with spaces
     * @param input String to pad
     * @param totalLength How long you want the string to be
     * @return padded String or original input if totalLength < input length
     */
    private static String padStringLeft(String input, int totalLength) {
        int padSize = totalLength - input.length();

        if (padSize <= 0)
            return input;

        return " ".repeat(padSize) + input;
    }

    /**
     * Will pad a string on the right side with spaces
     * @param input String to pad
     * @param totalLength How long you want the string to be
     * @return padded String or original input if totalLength < input length
     */
    private static String padStringRight(String input, int totalLength) {
        int padSize = totalLength - input.length();

        if (padSize <= 0)
            return input;

        return input + " ".repeat(padSize);
    }

    /**
     * Extracts played cards from playerInfo hashtable
     * @param playerInfo Output of getPlayerInfo()
     * @return Entries have index of discard order as key and the card info as value
     */
    private static Hashtable<String,String> getPlayedCardsHashtable(Hashtable<String,String> playerInfo) {
        Hashtable<String,String> playedCards = new Hashtable<>();

        for (String key: playerInfo.keySet()) {
            if (key.startsWith("playedCards_")) {
                String shortKey = key.replace("playedCards_", "");
                playedCards.put(shortKey, playerInfo.get(key));
            }
        }

        return playedCards;
    }

    /**
     * Creates a String that will display the amount of hearts as red background in terminal.
     * @param numberOfHearts The amount of hearts the string is going to display
     * @param lineSize The length the line should have when displayed in terminal
     * @return String to display the hearts in terminal als red boxes
     */
    private String CreatePlayerHeartLine(int numberOfHearts, int lineSize) {
        int spaceHearts;

        if (numberOfHearts == 0)
            spaceHearts = 0;
        else
            spaceHearts = 2 * numberOfHearts + (numberOfHearts - 1);

        int padding = lineSize - spaceHearts;
        int leftPadding = padding/ 2;
        int rightPadding = padding - leftPadding;
        String hearts = "";

        for (int i = 0 ; i < numberOfHearts; i++) {
            hearts += HEART;

            if (i < numberOfHearts - 1)
                hearts += " ";
        }

        return " ".repeat(leftPadding) + hearts + " ".repeat(rightPadding);
    }
}
package LoveLetter;

/**
 * LoveLetter - The SEP1 project for Annette's Heart.
 * PlayGame is the entry point of the project
 * @author Felix Paesler
 * @version 1.0
 */
public class PlayGame {
    public static void main(String[] args) {
        Model_Game modelGame = new Model_Game();
        ViewModel viewModel = new ViewModel(modelGame);
        View view = new View(viewModel);
        view.display();
    }
}
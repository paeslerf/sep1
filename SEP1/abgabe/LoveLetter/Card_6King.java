package LoveLetter;

import java.util.ArrayList;

/**
 * King
 * @hidden
 */
public class Card_6King extends ACard {
    private static final String _name = "Arnaud IV";
    private static final String _role = "King";
    private static final int _value = 6;
    private static final String _effect = "Trade card with someone.";
    public Card_6King(DeckOfCards deck) { Deck = deck; }

    @Override
    public String getName()
    {
        return _name;
    }

    @Override
    public String getRole() { return _role; }

    @Override
    public int getValue()
    {
        return _value;
    }

    @Override
    public String getEffect() {return _effect;}

    @Override
    public void play()
    {
        switch (getNextAction()) {
            case pickCard -> {
                if (Deck.Game.OtherCard instanceof Card_7Countess) {
                    ACard tmp = Deck.Game.PickedCard;
                    Deck.Game.PickedCard = Deck.Game.OtherCard;
                    Deck.Game.OtherCard = tmp;
                    Deck.Game.PickedCard.play();
                    break;
                }

                ArrayList<Player> potentialTargets = Deck.Game.getOtherPlayersInPlayUnprotected();
                potentialTargets.remove(Deck.Game.CurrentPlayer);

                if (potentialTargets.size() == 0) {
                    discard();
                    Deck.Game.Message = "You discard. There are no other unprotected players in game.";
                    Deck.Game.NextAction = Action.endTurn;
                    Deck.Game.isRoundOverRoutine();
                } else if (potentialTargets.size() == 1) {
                    Deck.Game.SelectedPlayer = potentialTargets.get(0);
                    Deck.Game.SelectedPlayer.CardSwitchingPartner = Deck.Game.CurrentPlayer;
                    Deck.Game.CurrentPlayer.CardSwitchingPartner = Deck.Game.SelectedPlayer;
                    ACard ownCard = Deck.Game.OtherCard;
                    Deck.Game.CurrentPlayer.CardGivenAway = ownCard;
                    Deck.Game.SelectedPlayer.CardGotten = ownCard;
                    ACard otherPlayersCard = Deck.Game.SelectedPlayer.Cards.get(0);
                    Deck.Game.CurrentPlayer.CardGotten = otherPlayersCard;
                    Deck.Game.SelectedPlayer.CardGivenAway = otherPlayersCard;
                    Deck.Game.CurrentPlayer.Cards.clear();
                    Deck.Game.SelectedPlayer.Cards.clear();
                    Deck.Game.CurrentPlayer.Cards.add(otherPlayersCard);
                    Deck.Game.SelectedPlayer.Cards.add(ownCard);
                    otherPlayersCard.Holder = Deck.Game.CurrentPlayer;
                    ownCard.Holder = Deck.Game.SelectedPlayer;
                    discard();
                    Deck.Game.Message = "You switched card with " + Deck.Game.SelectedPlayer.Name + '.';
                    Deck.Game.CurrentRightCard = null;
                    Deck.Game.CurrentLeftCard = Deck.Game.CurrentPlayer.Cards.get(0);
                    Deck.Game.NextAction = Action.endTurn;
                    Deck.Game.isRoundOverRoutine();
                } else {
                    Deck.Game.NextAction = Action.pickOtherUnprotectedPlayerInPlay;
                    Deck.Game.Message = "Who do you want to trade your card with?";
                }
            }
            case pickOtherUnprotectedPlayerInPlay -> {
                Deck.Game.SelectedPlayer.CardSwitchingPartner = Deck.Game.CurrentPlayer;
                Deck.Game.CurrentPlayer.CardSwitchingPartner = Deck.Game.SelectedPlayer;
                ACard ownCard = Deck.Game.OtherCard;
                Deck.Game.CurrentPlayer.CardGivenAway = ownCard;
                Deck.Game.SelectedPlayer.CardGotten = ownCard;
                ACard otherPlayersCard = Deck.Game.SelectedPlayer.Cards.get(0);
                Deck.Game.CurrentPlayer.CardGotten = otherPlayersCard;
                Deck.Game.SelectedPlayer.CardGivenAway = otherPlayersCard;
                Deck.Game.CurrentPlayer.Cards.clear();
                Deck.Game.SelectedPlayer.Cards.clear();
                Deck.Game.CurrentPlayer.Cards.add(otherPlayersCard);
                Deck.Game.SelectedPlayer.Cards.add(ownCard);
                otherPlayersCard.Holder = Deck.Game.CurrentPlayer;
                ownCard.Holder = Deck.Game.SelectedPlayer;
                discard();
                Deck.Game.Message = "You switched card with " + Deck.Game.SelectedPlayer.Name + '.';
                Deck.Game.CurrentRightCard = null;
                Deck.Game.CurrentLeftCard = Deck.Game.CurrentPlayer.Cards.get(0);
                Deck.Game.NextAction = Action.endTurn;
                Deck.Game.isRoundOverRoutine();
            }
            case endTurn -> Deck.Game.nextPlayerTurn();
        }
    }
}

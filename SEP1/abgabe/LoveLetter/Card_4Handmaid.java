package LoveLetter;

/**
 * Handmaid
 * @hidden
 */
public class Card_4Handmaid extends ACard {
    private static final String _name = "Susannah";
    private static final String _role = "Handmaid";
    private static final int _value = 4;
    private static final String _effect = "Protects you from other players for one round.";

    public Card_4Handmaid(DeckOfCards deck) { Deck = deck; }

    @Override
    public String getName()
    {
        return _name;
    }

    @Override
    public String getRole() { return _role; }

    @Override
    public int getValue()
    {
        return _value;
    }

    @Override
    public String getEffect() {return _effect;}

    @Override
    public void play()
    {
        switch (getNextAction()) {
            case pickCard -> {
                Deck.Game.CurrentPlayer.IsProtected = true;
                discard();
                Deck.Game.Message = "You are protected till your next turn.";
                Deck.Game.NextAction = Action.endTurn;
                Deck.Game.isRoundOverRoutine();
            }
            case endTurn -> Deck.Game.nextPlayerTurn();
        }
    }
}

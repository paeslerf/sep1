package LoveLetter;

import java.util.*;

/**
 * Connects game logic to view and handles intermediary work
 */
public class ViewModel
{
    private final Model_Game _modelGame;
    private Action _nextAction;
    private Action _externalAction;
    private boolean _running;
    private boolean _showScore;
    private String _message;

    /**
     * Creates ViewModel with reference to a game (model)
     * @param game Game (model) containing the actual gameplay
     */
    public ViewModel(Model_Game game)
    {
        _modelGame = game;
        _running = true;
        _nextAction = Action.start;
        _showScore = false;
    }

    /**
     * Tells if the programm should be running. (If false, it will be shut down)
     * @return true as long as \exit has not been entered
     */
    public boolean isStillRunning() {return _running;}

    /**
     * Updates message attribute by copying game messages
     */
    public void updateMessage() { _message = _modelGame.Message; }

    /**
     * Updates next expected action
     */
    public void updateNextAction() { _nextAction = _modelGame.NextAction; }

    /**
     * Tells whether score should be shown or be hidden
     * @return true if score should be shown, false otherwise
     */
    public boolean getShowScore() {return _showScore;}

    /**
     * Gets message currently saved in ViewModel
     * @return Message saved in ViewModel
     */
    public String getMessage() { return _message;}

    /**
     * Tells how many players participate in game
     * @return number of players taking part
     */
    public int getNumberOfPlayers() { return _modelGame.NumberOfPlayer;}

    /**
     * Gets number of current round
     * @return number of current round starting with 0
     */
    public int getRoundNumber() { return _modelGame.getRoundNumber(); }

    /**
     * Gets name of current player
     * @return Name of current player
     */
    public String getCurrentPlayerName() { return _modelGame.CurrentPlayer.Name; }

    /**
     * Called to determine what screen should be displayed.
     * @return LoveLetter.Action state of display (i.e. help-screen or regular game-screen)
     */
    public Action getActionState() {
        if (_externalAction == null)
            return _nextAction;
        else
            return _externalAction;
    }

    /**
     * Validates if a String is a valid input for the current NextAction.
     * @param input Any String
     * @return true if String was valid, false otherwise
     */
    public boolean isValidInput(String input) {
        updateNextAction();
        Command command = convertStringToCommand(input);
        return _modelGame.isValidCommand(command);
    }

    /**
     * Converts any string into a command
     * @param input Any String
     * @return LoveLetter.Command according to string. Unknown strings will be converted to noCommand.
     */
    private Command convertStringToCommand(String input) {
        Command command = Command.noCommand;

        if ("\\exit".equals(input))
            return Command.exit;

        if ("\\help".equals(input))
            return Command.help;

        if (_nextAction == Action.enterPlayerNumber) {
            if (isInt(input)&& _modelGame.isValidNumberOfPlayers(Integer.parseInt(input))) {
                int numberOfPlayers =  Integer.parseInt(input);
                command = Command.enterPlayerNumber_2;

                if (numberOfPlayers == 3)
                    command = Command.enterPlayerNumber_3;

                if (numberOfPlayers == 4)
                    command = Command.enterPlayerNumber_4;

                return command;
            }
        }

        if (_nextAction == Action.enterPlayerName){
            if (_modelGame.isValidPlayerName(input))
                command = Command.enterPlayerName;

            return command;
        }

        if ("\\playCard".equals(input)) {
            Random rd = new Random();

            if (rd.nextBoolean())
                input = "\\left";
            else
                input = "\\right";
        }

        switch (input) {
            case "\\left" -> command = Command.pickCard_left;
            case "\\right" -> command = Command.pickCard_right;
            case "\\showHand", "\\startTurn", "\\showPlayers" -> command = Command.drawCard;
            case "\\showScore" -> command = Command.showScore;
            case "\\endTurn" -> command = Command.endTurn;
            case "\\newGame" -> command = Command.newGame;
            case "\\start" -> command = Command.start;
            case "2" -> command = Command.pickValue_2;
            case "3" -> command = Command.pickValue_3;
            case "4" -> command = Command.pickValue_4;
            case "5" -> command = Command.pickValue_5;
            case "6" -> command = Command.pickValue_6;
            case "7" -> command = Command.pickValue_7;
            case "8" -> command = Command.pickValue_8;
        }

        Hashtable<String,Command> pickPlayerNameTable = _modelGame.getCommandForPickPlayerPerNameHashtable();

        if (pickPlayerNameTable.containsKey(input))
            command = pickPlayerNameTable.get(input);

        return command;
    }

    /**
     * Checks if a String can be converted to an integer
     * @param input Any String
     * @return true if String can be converted, false otherwise
     */
    private boolean isInt(String input) {
        if (input == null)
            return false;
        try {
            Integer.parseInt(input);
        } catch (NumberFormatException nfe) {
            return  false;
        }
        return true;
    }

    /**
     * Takes a string, converts it to a command and manipulates state of game accordingly.
     * @param input Any String
     */
    public void insertInput(String input) {
        Command command = convertStringToCommand(input);

        if (command == Command.exit) {
            _running = false;
            return;
        }

        if (command == Command.help) {
            if (_externalAction == null)
                _externalAction = Action.help;
            else
                _externalAction = null;

            updateMessage();
            return;
        }

        if (_modelGame.isValidCommand(command) && command == Command.showScore) {
            _showScore = !_showScore;
            return;
        }

        if(_modelGame.isValidCommand(command)) {
            _externalAction = null;
            _modelGame.doNextAction(command);
        }

        updateMessage();
        updateNextAction();
    }

    /**
     * Return player name given on current location of player in play order.
     * @param i 0 for current player, 1 for next and so on
     * @return Name of player at the given position
     */
    public String getPlayerName(int i) {
        if (0 <= i && i < _modelGame.NumberOfPlayer && i < _modelGame.Players.size())
            return _modelGame.Players.get(i).Name;
        else
            return "";
    }

    /**
     * Returns the number of players already added to the game.
     * @return The amount of players added to the game
     */
    public int getNumberOfCreatedPlayers() {
        if (_modelGame.Players == null)
            return 0;
        else
            return _modelGame.Players.size();
    }

    /**
     * Get a list of all items to be displayed in help menu for a given state of the game.
     * @return List containing two-dimensional String arrays
     */
    public ArrayList<String[]> GetHelpOptions() {
        ArrayList<String[]> helpList = new ArrayList<>();
        updateNextAction();

        if (_nextAction == Action.enterPlayerNumber) {
            String[] playerNumberHelp = new String[2];
            playerNumberHelp[0] = "zzZZzzZ";
            playerNumberHelp[1] = "I'm waiting for you to enter a number between 2 and 4.";
            helpList.add(playerNumberHelp);
        }

        if (_nextAction == Action.enterPlayerName) {
            String[] PlayerNameHelp = new String[2];
            PlayerNameHelp[0] = "Hello?";
            PlayerNameHelp[1] = "Would you be so kind to tell me the name you go by these days?";
            helpList.add(PlayerNameHelp);
        }

        if (_nextAction == Action.start) {
            String[] startHelp = new String[2];
            startHelp[0] = "\\start";
            startHelp[1] = "Will lead you to enter the game for the princesses heart.";
            helpList.add(startHelp);
        }

        if(_nextAction == Action.newRound || _nextAction == Action.drawCard) {
            String[] startTurnHelp = new String[2];
            startTurnHelp[0] = "\\startTurn";
            startTurnHelp[1] = "Starts your turn.";
            helpList.add(startTurnHelp);

            String[] showHandHelp = new String[2];
            showHandHelp[0] = "\\showHand";
            showHandHelp[1] = "Starts your turn.";
            helpList.add(showHandHelp);

            String[] showPlayersHelp = new String[2];
            showPlayersHelp[0] = "\\showPlayers";
            showPlayersHelp[1] = "Starts your turn.";
            helpList.add(showPlayersHelp);
        }

        if (_nextAction == Action.pickCard) {
            String[] pickLeftCard = new String[2];
            pickLeftCard[0] = "\\left";
            pickLeftCard[1] = "Play the left card.";
            helpList.add(pickLeftCard);

            String[] pickRightCard = new String[2];
            pickRightCard[0] = "\\right";
            pickRightCard[1] = "Play the right card.";
            helpList.add(pickRightCard);

            String[] playHelp = new String[2];
            playHelp[0] = "\\playCard";
            playHelp[1] = "Play a random card.";
            helpList.add(playHelp);

            String[] scoreHelp = new String[2];
            scoreHelp[0] = "\\showScore";

            if (_showScore)
                scoreHelp[1] = "Can't handle the heat? You don't have to see all the love.";
            else
                scoreHelp[1] = "If you really want to know who is fancied by the princess.";

            helpList.add(scoreHelp);

            if (_modelGame.CurrentLeftCard != null) {
                String[] leftCardHelp = new String[2];
                leftCardHelp[0] = _modelGame.CurrentLeftCard.getRole();
                leftCardHelp[1] = _modelGame.CurrentLeftCard.getEffect();
                helpList.add(leftCardHelp);
            }

            if (_modelGame.CurrentRightCard != null){
                String[] rightCardHelp = new String[2];
                rightCardHelp[0] = _modelGame.CurrentRightCard.getRole();
                rightCardHelp[1] = _modelGame.CurrentRightCard.getEffect();
                helpList.add(rightCardHelp);
            }

        }

        if (_nextAction == Action.pickOtherUnprotectedPlayerInPlay) {
            String[] pickOtherUnprotectedPlayerInPlay = new String[2];
            pickOtherUnprotectedPlayerInPlay[0] = "<name>";
            pickOtherUnprotectedPlayerInPlay[1] = "Pick another unprotected player in game.";
            helpList.add(pickOtherUnprotectedPlayerInPlay);

            String[] inGameExplanation = new String[2];
            inGameExplanation[0] = "Out of Round";
            inGameExplanation[1] = "LoveLetter.Player name is grey";
            helpList.add(inGameExplanation);

            String[] protectedExplanation = new String[2];
            protectedExplanation[0] = "Protected";
            protectedExplanation[1] = "LoveLetter.Player name is underscored.";
            helpList.add(protectedExplanation);

            String[] scoreHelp = new String[2];
            scoreHelp[0] = "\\showScore";

            if (_showScore)
                scoreHelp[1] = "Can't handle the heat? You don't have to see all the love.";
            else
                scoreHelp[1] = "If you really want to know who is fancied by the princess.";

            helpList.add(scoreHelp);

            if (_modelGame.CurrentLeftCard != null) {
                String[] leftCardHelp = new String[2];
                leftCardHelp[0] = _modelGame.CurrentLeftCard.getRole();
                leftCardHelp[1] = _modelGame.CurrentLeftCard.getEffect();
                helpList.add(leftCardHelp);
            }

            if (_modelGame.CurrentRightCard != null){
                String[] rightCardHelp = new String[2];
                rightCardHelp[0] = _modelGame.CurrentRightCard.getRole();
                rightCardHelp[1] = _modelGame.CurrentRightCard.getEffect();
                helpList.add(rightCardHelp);
            }
        }

        if (_nextAction == Action.pickUnprotectedPlayerInPlay)
        {
            String[] pickUnprotectedPlayerInPlay = new String[2];
            pickUnprotectedPlayerInPlay[0] = "<name>";
            pickUnprotectedPlayerInPlay[1] = "Pick an unprotected player in game or yourself.";
            helpList.add(pickUnprotectedPlayerInPlay);

            String[] inGameExplanation = new String[2];
            inGameExplanation[0] = "Out of Round";
            inGameExplanation[1] = "LoveLetter.Player name is grey";
            helpList.add(inGameExplanation);

            String[] protectedExplanation = new String[2];
            protectedExplanation[0] = "Protected";
            protectedExplanation[1] = "LoveLetter.Player name is underscored.";
            helpList.add(protectedExplanation);

            String[] scoreHelp = new String[2];
            scoreHelp[0] = "\\showScore";

            if (_showScore)
                scoreHelp[1] = "Can't handle the heat? You don't have to see all the love.";
            else
                scoreHelp[1] = "If you really want to know who is fancied by the princess.";
            helpList.add(scoreHelp);

            if (_modelGame.CurrentLeftCard != null) {
                String[] leftCardHelp = new String[2];
                leftCardHelp[0] = _modelGame.CurrentLeftCard.getRole();
                leftCardHelp[1] = _modelGame.CurrentLeftCard.getEffect();
                helpList.add(leftCardHelp);
            }

            if (_modelGame.CurrentRightCard != null){
                String[] rightCardHelp = new String[2];
                rightCardHelp[0] = _modelGame.CurrentRightCard.getRole();
                rightCardHelp[1] = _modelGame.CurrentRightCard.getEffect();
                helpList.add(rightCardHelp);
            }
        }

        if (_nextAction == Action.pickValueNot1) {
            String[] pickValueHelp = new String[2];
            pickValueHelp[0] = "<2>...<8>";
            pickValueHelp[1] = "Guess a card by value between 2 and 8.";
            helpList.add(pickValueHelp);

            String[] scoreHelp = new String[2];
            scoreHelp[0] = "\\showScore";

            if (_showScore)
                scoreHelp[1] = "Can't handle the heat? You don't have to see all the love.";
            else
                scoreHelp[1] = "If you really want to know who is fancied by the princess.";

            helpList.add(scoreHelp);

            if (_modelGame.CurrentLeftCard != null) {
                String[] leftCardHelp = new String[2];
                leftCardHelp[0] = _modelGame.CurrentLeftCard.getRole();
                leftCardHelp[1] = _modelGame.CurrentLeftCard.getEffect();
                helpList.add(leftCardHelp);
            }

            if (_modelGame.CurrentRightCard != null){
                String[] rightCardHelp = new String[2];
                rightCardHelp[0] = _modelGame.CurrentRightCard.getRole();
                rightCardHelp[1] = _modelGame.CurrentRightCard.getEffect();
                helpList.add(rightCardHelp);
            }
        }

        if (_nextAction == Action.endTurn) {
            String[] endTurnHelp = new String[2];
            endTurnHelp[0] = "\\endTurn";
            endTurnHelp[1] = "Ends your turn.";
            helpList.add(endTurnHelp);

            String[] scoreHelp = new String[2];
            scoreHelp[0] = "\\showScore";

            if (_showScore)
                scoreHelp[1] = "Can't handle the heat? You don't have to see all the love.";
            else
                scoreHelp[1] = "If you really want to know who is fancied by the princess.";

            helpList.add(scoreHelp);
        }

        String[] leaveHelp = new String[2];
        leaveHelp[0] = "\\help";
        leaveHelp[1] = "Come to or leave the \"Library of Dead Poets\".";
        helpList.add(leaveHelp);

        String[] exitHelp = new String[2];
        exitHelp[0] = "\\exit";
        exitHelp[1] = "Nobody forces you to be here. You can always quit.";
        helpList.add(exitHelp);

        return helpList;
    }

    /**
     * Get the names of all players who can currently be picked
     * @return ArrayList of all names that can be picked
     */
    public ArrayList<String> getPickablePlayerNames(){
        ArrayList<String> playerNames = new ArrayList<>();
        ArrayList<Player> players;

        if (_nextAction == Action.pickUnprotectedPlayerInPlay) {
            players = _modelGame.getPlayersInPlayUnprotected();

            for (Player player: players)
                playerNames.add(player.Name);
        }

        if (_nextAction == Action.pickOtherUnprotectedPlayerInPlay) {
            players = _modelGame.getOtherPlayersInPlayUnprotected();

            for (Player player: players)
                playerNames.add(player.Name);
        }

        return  playerNames;
    }


    /**
     * Get the name of the last winner of a round.
     * @return Name of player or empty string if there hasn't been a winner yet.
     */
    public String getWinnerName() {
        if (_modelGame.WinnerLastRound == null)
            return "";
        else
            return _modelGame.WinnerLastRound.Name;
    }

    /**
     * Get all the info for a player in a hashtable.
     * @param position 0 for current player, 1 for next player and so on. Position is not verified so only use for
     *                 players you know exist
     * @return Hashtable containing keys: name, hearts, inPlay, isProtected, selectedPlayer and playedCards_ with an
     * added index
     */
    public Hashtable<String,String> getPlayerInfo(int position)
    {
        Hashtable<String,String> playerInfo = new Hashtable<>();
        Player player= _modelGame.Players.get(position);
        playerInfo.put("name", player.Name);
        playerInfo.put("hearts", String.valueOf(player.Hearts));
        playerInfo.put("inPlay", String.valueOf(player.InPlay));
        playerInfo.put("isProtected", String.valueOf(player.IsProtected));

        for(int i = 1; i <= player.PlayedCards.size(); i++)
            playerInfo.put("playedCards_" + i, player.PlayedCards.get(i - 1).getRole() + "  ("
                    + player.PlayedCards.get(i - 1).getValue()+ ")");

        if (player == _modelGame.SelectedPlayer) {
            playerInfo.put("selectedPlayer", "true");
        } else {
            playerInfo.put("selectedPlayer", "false");
        }

        return playerInfo;
    }

    /**
     * Get the side of the card the current player picked.
     * @return left, right, or an empty string
     */
    public String getSideOfPickedCard() {
        if (_modelGame.PickedCard == _modelGame.CurrentLeftCard && _modelGame.PickedCard != null)
            return "left";

        if (_modelGame.PickedCard == _modelGame.CurrentRightCard && _modelGame.PickedCard != null)
            return "right";

        return "";
    }

    /**
     * Get all info about the table from perspective of current player. This includes left and right card,
     * picked card, open cards in two player game remaining deck size, side card and the message.
     * @return Will have the entries: leftCard_Value, leftCard_Role, rightCard_Value, rightCard_Role, pickedCard,
     * remainingDeckSize, sideCard, message. In two player games also: openCard1, openCard2, openCard3
     */
    public Hashtable<String,String> getGameInfo() {
        Hashtable<String,String> gameInfo = new Hashtable<>();

        if (_modelGame.CurrentLeftCard == null) {
            gameInfo.put("leftCard_Value", "");
            gameInfo.put("leftCard_Role", "");
        } else {
            gameInfo.put("leftCard_Value", String.valueOf(_modelGame.CurrentLeftCard.getValue()));
            gameInfo.put("leftCard_Role", _modelGame.CurrentLeftCard.getRole());
        }

        if (_modelGame.CurrentRightCard == null) {
            gameInfo.put("rightCard_Value", "");
            gameInfo.put("rightCard_Role", "");
        } else {
            gameInfo.put("rightCard_Value", String.valueOf(_modelGame.CurrentRightCard.getValue()));
            gameInfo.put("rightCard_Role", _modelGame.CurrentRightCard.getRole());
        }

        gameInfo.put("pickedCard", getSideOfPickedCard());

        if (_modelGame.NumberOfPlayer == 2) {
            gameInfo.put("openCard1", _modelGame.OpenCardsFor2Players.get(0).getValue() + " "
                    + _modelGame.OpenCardsFor2Players.get(0).getRole());
            gameInfo.put("openCard2", _modelGame.OpenCardsFor2Players.get(1).getValue() + " "
                    + _modelGame.OpenCardsFor2Players.get(1).getRole());
            gameInfo.put("openCard3", _modelGame.OpenCardsFor2Players.get(2).getValue() + " "
                    + _modelGame.OpenCardsFor2Players.get(2).getRole());
        }

        gameInfo.put("remainingDeckSize", String.valueOf(_modelGame.getRemainingDeckSize()));

        if (_modelGame.SideCard != null)
            gameInfo.put("sideCard", "1");

        gameInfo.put("message", _modelGame.Message);

        return gameInfo;
    }

    /**
     * Returns a message describing a trade of cards if one has happened to the current player
     * @return message about traded cards or empty String
     */
    public String getActivePlayerCardTrades() {
        String tradedCards = "";
        if (_modelGame.CurrentPlayer.CardSwitchingPartner != null)
            tradedCards = "You gave " + _modelGame.CurrentPlayer.CardSwitchingPartner.Name
                    + " a " + _modelGame.CurrentPlayer.CardGivenAway.getRole() + " and got a "
                    + _modelGame.CurrentPlayer.CardGotten.getRole() + ".";

        return tradedCards;
    }
}

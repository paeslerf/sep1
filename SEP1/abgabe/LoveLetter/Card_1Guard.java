package LoveLetter;

import java.util.ArrayList;

/**
 * Guard
 * @hidden
 */
public class Card_1Guard extends ACard {
    private static final String _name = "Odette";
    private static final String _role = "Guard";
    private static final int _value = 1;
    private static final String _effect = "Guess another players card and they're out!";

    public Card_1Guard(DeckOfCards deck)
    {
        Deck = deck;
    }

    @Override
    public String getName()
    {
        return _name;
    }

    @Override
    public String getRole() { return _role; }

    @Override
    public int getValue()
    {
        return _value;
    }

    @Override
    public String getEffect() {return _effect;}

    @Override
    public void play()
    {
        switch (getNextAction()) {
            case pickCard -> {
                ArrayList<Player> potentialTargets = Deck.Game.getOtherPlayersInPlayUnprotected();
                potentialTargets.remove(Deck.Game.CurrentPlayer);

                if (potentialTargets.size() == 0) {
                    discard();
                    Deck.Game.Message = "No potential targets, so no effect.";
                    Deck.Game.NextAction = Action.endTurn;
                    Deck.Game.isRoundOverRoutine();
                } else if (potentialTargets.size() == 1) {
                    Deck.Game.SelectedPlayer = potentialTargets.get(0);
                    Deck.Game.NextAction = Action.pickValueNot1;
                    Deck.Game.Message = "What card does " + Deck.Game.SelectedPlayer.Name + " have?";
                } else {
                    Deck.Game.NextAction = Action.pickOtherUnprotectedPlayerInPlay;
                    Deck.Game.Message = "Whose card do you want to guess?";
                }
            }
            case pickOtherUnprotectedPlayerInPlay -> {
                Deck.Game.NextAction = Action.pickValueNot1;
                Deck.Game.Message = "What card does " + Deck.Game.SelectedPlayer.Name + " have?";
            }
            case pickValueNot1 -> {
                discard();

                if (Deck.Game.SelectedPlayer.Cards.get(0).getValue() == Deck.Game.GuessValue) {
                    Deck.Game.SelectedPlayer.InPlay = false;
                    Deck.Game.SelectedPlayer.Cards.get(0).discard();
                    Deck.Game.Message = "One less lovebird in the game!";
                } else
                    Deck.Game.Message = "Sorry, you guessed wrong.";

                Deck.Game.NextAction = Action.endTurn;
                Deck.Game.isRoundOverRoutine();
            }
            case endTurn -> Deck.Game.nextPlayerTurn();
        }
    }
}

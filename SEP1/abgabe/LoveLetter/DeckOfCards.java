package LoveLetter;

import java.util.*;

/**
 * Collection of cards needed to play the game
 */
public class DeckOfCards {
    /**
     * Cards in deck
     */
    public List<ACard> Cards;
    /**
     * Game this deck belongs to
     */
    public Model_Game Game;

    /**
     * Creates a deck of cards for a game
     * @param game Game this deck belongs to
     */
    public DeckOfCards(Model_Game game) {
        Game = game;
        Cards = new ArrayList<>();
        shuffleCards();
    }

    /**
     * Shuffle the whole deck of cards
     */
    public void shuffleCards() {
        _fillDeck();
        Collections.shuffle(Cards);
    }

    /**
     * Puts together the right amount of each card in a deck
     */
    private void _fillDeck() {
        Cards.clear();
        Cards.add(new Card_8Princess(this));
        Cards.add(new Card_7Countess(this));
        Cards.add(new Card_6King(this));

        for(int i = 0; i < 2; i++) {
            Cards.add(new Card_5Prince(this));
            Cards.add(new Card_4Handmaid(this));
            Cards.add(new Card_3Baron(this));
            Cards.add(new Card_2Priest(this));
        }

        for(int i = 0; i < 5; i++)
            Cards.add((new Card_1Guard(this)));
    }

    /**
     * Draw a card from top of the deck
     * @return First card of Cards or null if deck is empty
     */
    public ACard drawCard() {
        if (hasCards()) {
            ACard topCard = Cards.get(0);
            Cards.remove(0);
            return topCard;
        }
        else
            return  null;
    }

    /**
     * Check if there are still cards in the deck.
     * @return true if there are cards in the deck. false if deck is empty.
     */
    public boolean hasCards() {
        return Cards.size() > 0;
    }
}

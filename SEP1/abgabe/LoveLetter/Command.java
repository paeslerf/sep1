package LoveLetter;

/**
 * Commands for navigating the game logic
 * @hidden
 */
public enum Command {
    // basic commands
    start, help, exit, showScore,

    // setup commands
    enterPlayerNumber_2, enterPlayerNumber_3, enterPlayerNumber_4, enterPlayerName,

    // starting your round
    drawCard,

    // choosing card
    pickCard_left, pickCard_right,

    // choosing player
    pickPlayer_0, pickPlayer_1, pickPlayer_2, pickPlayer_3,

    // guess card value
    pickValue_2, pickValue_3, pickValue_4, pickValue_5, pickValue_6, pickValue_7, pickValue_8,

    // end turn to waiting screen
    endTurn,

    // end game command
    newGame,

    // dummy command
    noCommand
}

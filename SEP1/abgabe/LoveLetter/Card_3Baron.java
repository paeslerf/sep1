package LoveLetter;

import java.util.ArrayList;

/**
 * Baron
 * @hidden
 */
public class Card_3Baron extends ACard {
    private static final String _name = "Talus";
    private static final String _role = "Baron";
    private static final int _value = 3;
    private static final String _effect = "Pick someone to compare values with. Loser is out.";
    public Card_3Baron(DeckOfCards deck) { Deck = deck; }

    @Override
    public String getName()
    {
        return _name;
    }

    @Override
    public String getRole() { return _role; }

    @Override
    public int getValue()
    {
        return _value;
    }

    @Override
    public String getEffect() {return _effect;}

    @Override
    public void play()
    {
        switch (getNextAction()) {
            case pickCard -> {
                ArrayList<Player> potentialTargets = Deck.Game.getOtherPlayersInPlayUnprotected();
                potentialTargets.remove(Deck.Game.CurrentPlayer);

                if (potentialTargets.size() == 0) {
                    discard();
                    Deck.Game.Message = "No potential targets. Nothing happens.";
                    Deck.Game.NextAction = Action.endTurn;
                    Deck.Game.isRoundOverRoutine();
                } else if (potentialTargets.size() == 1) {
                    Deck.Game.SelectedPlayer = potentialTargets.get(0);
                    int ownCardValue = Deck.Game.OtherCard.getValue();
                    int opponentsCardValue = Deck.Game.SelectedPlayer.Cards.get(0).getValue();
                    Deck.Game.Message = "You could only pick " + Deck.Game.SelectedPlayer.Name + '.';

                    if (ownCardValue > opponentsCardValue) {
                        Deck.Game.SelectedPlayer.InPlay = false;
                        Deck.Game.SelectedPlayer.Cards.get(0).discard();
                        Deck.Game.Message += " They had the lower card.";
                    } else if (ownCardValue < opponentsCardValue) {
                        Deck.Game.CurrentPlayer.InPlay = false;
                        Deck.Game.OtherCard.discard();
                        Deck.Game.Message += " You had the lower card.";
                    } else
                        Deck.Game.Message += " It was a draw";

                    discard();
                    Deck.Game.NextAction = Action.endTurn;
                    Deck.Game.isRoundOverRoutine();
                } else {
                    Deck.Game.NextAction = Action.pickOtherUnprotectedPlayerInPlay;
                    Deck.Game.Message = "Who do you want to compare cards with?";
                }
            }
            case pickOtherUnprotectedPlayerInPlay -> {
                int ownCardValue = Deck.Game.OtherCard.getValue();
                int opponentsCardValue = Deck.Game.SelectedPlayer.Cards.get(0).getValue();

                if (ownCardValue > opponentsCardValue) {
                    Deck.Game.SelectedPlayer.InPlay = false;
                    Deck.Game.SelectedPlayer.Cards.get(0).discard();
                    Deck.Game.Message = "Your card won. They are out.";
                } else if (ownCardValue < opponentsCardValue) {
                    Deck.Game.CurrentPlayer.InPlay = false;
                    Deck.Game.OtherCard.discard();
                    Deck.Game.Message = "Tough luck. You're out.";
                } else
                    Deck.Game.Message = "It's a draw. Nobody is out.";

                discard();
                Deck.Game.NextAction = Action.endTurn;
                Deck.Game.isRoundOverRoutine();
            }
            case endTurn -> Deck.Game.nextPlayerTurn();
        }
    }
}

package LoveLetter;

/**
 * Princess
 * @hidden
 */
public class Card_8Princess extends ACard {
    private static final String _name = "Annette";
    private static final String _role = "Princess";
    private static final int _value = 8;

    private static final String _effect = "If discarded holder is out.";
    public Card_8Princess(DeckOfCards deck)
    {
        Deck = deck;
    }

    /**
     * Princess will take holder out of the game when discarded, so we overwrite the base method.
     */
    public void discard() {
        Holder.Cards.remove(this);
        Holder.PlayedCards.add(this);
        Holder.InPlay = false;
    }

    @Override
    public String getName()
    {
        return _name;
    }

    @Override
    public String getRole() { return _role; }

    @Override
    public int getValue()
    {
        return _value;
    }

    @Override
    public String getEffect() {return _effect;}

    @Override
    public void play() {
        switch (getNextAction()) {
            case pickCard -> {
                discard();
                Deck.Game.OtherCard.discard();
                Deck.Game.Message = "Playing hard to get doesn't play well with nobility!";
                Deck.Game.NextAction = Action.endTurn;
                Deck.Game.isRoundOverRoutine();
            }
            case endTurn -> Deck.Game.nextPlayerTurn();
        }
    }
}

package LoveLetter;

/**
 * Properties and methods every card needs to have or implement
 */
public abstract class ACard {
    /**
     * Who has this card in their hand
     */
    public Player Holder;
    /**
     * Deck this card is a part of
     */
    public DeckOfCards Deck;

    /**
     * Get name of a cards character
     * @return Name attribute of card
     */
    public abstract String getName();

    /**
     * Get role attribute of a card
     * @return Role attribute of card
     */
    public abstract String getRole();

    /**
     * Get value attribute of a card
     * @return Value of card
     */
    public abstract int getValue();

    /**
     * Get description of effect the card has
     * @return Explanation of card effect
     */
    public abstract String getEffect();

    /**
     * Will put card effects into motion depending on game context
     */
    public abstract void play();

    /**
     * Discard a card by playing it or being forced through other means. Basis for remembering played cards.
     */
    public void discard() {
        Holder.Cards.remove(this);
        Holder.PlayedCards.add(this);
    }

    /**
     * Used to get next action of game. Used for better readability
     * @return nextAction of game
     */
    public Action getNextAction() {

        return Deck.Game.NextAction;
    }
}

package LoveLetter;

import java.util.ArrayList;

/**
 * Priest
 * @hidden
 */
public class Card_2Priest extends ACard {
    private static final String _name = "Tomas";
    private static final String _role = "Priest";
    private static final int _value = 2;
    private static final String _effect = "Let's you see someones card.";

    public Card_2Priest(DeckOfCards deck)
    {
        Deck = deck;
    }

    @Override
    public String getName()
    {
        return _name;
    }

    @Override
    public String getRole() { return _role; }

    @Override
    public int getValue()
    {
        return _value;
    }

    @Override
    public String getEffect() {return _effect;}

    @Override
    public void play() {
        switch (getNextAction()) {
            case pickCard -> {
                ArrayList<Player> potentialTargets = Deck.Game.getOtherPlayersInPlayUnprotected();
                potentialTargets.remove(Deck.Game.CurrentPlayer);

                if (potentialTargets.size() == 0) {
                    discard();
                    Deck.Game.Message = "Everybody is either protected or out.";
                    Deck.Game.NextAction = Action.endTurn;
                    Deck.Game.isRoundOverRoutine();
                } else if (potentialTargets.size() == 1) {
                    Deck.Game.SelectedPlayer = potentialTargets.get(0);
                    Deck.Game.RevealedCard = Deck.Game.SelectedPlayer.Cards.get(0);
                    discard();
                    Deck.Game.Message = Deck.Game.SelectedPlayer.Name + " has the card: "
                            + Deck.Game.RevealedCard.getRole() + " ("
                            + Deck.Game.RevealedCard.getValue() + ")";
                    Deck.Game.NextAction = Action.endTurn;
                    Deck.Game.isRoundOverRoutine();
                } else {
                    Deck.Game.NextAction = Action.pickOtherUnprotectedPlayerInPlay;
                    Deck.Game.Message = "Whose card would you like to see?";
                }
            }
            case pickOtherUnprotectedPlayerInPlay -> {
                Deck.Game.RevealedCard = Deck.Game.SelectedPlayer.Cards.get(0);
                discard();
                Deck.Game.Message = Deck.Game.SelectedPlayer.Name + " has the card: "
                        + Deck.Game.RevealedCard.getRole() + " ("
                        + Deck.Game.RevealedCard.getValue() + ")";
                Deck.Game.NextAction = Action.endTurn;
                Deck.Game.isRoundOverRoutine();
            }
            case endTurn -> Deck.Game.nextPlayerTurn();
        }
    }
}

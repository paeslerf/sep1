package LoveLetter;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Contains the actual game Love Letter with all it's cards, players, win conditions and board states
 */
public class Model_Game {
    private final DeckOfCards _deck;
    /**
     * Amount of players
     */
    public int NumberOfPlayer;
    /**
     * The value a player guesses when using a Guard
     */
    public int GuessValue;
    /**
     * Spare card that is drawn if deck is empty, but someone is forced to draw
     */
    public ACard SideCard;
    /**
     * Cards shown to everyone when playing with only two players
     */
    public ArrayList<ACard> OpenCardsFor2Players;
    /**
     * Players taking part in order from current to last
     */
    public ArrayList<Player> Players;
    /**
     * Winner of the last round completed. Null when new game just started
     */
    public Player WinnerLastRound;
    /**
     * Player whose turn it is currently
     */
    public Player CurrentPlayer;
    /**
     * Player picked by current player
     */
    public Player SelectedPlayer;
    /**
     * Card revealed by priest
     */
    public ACard RevealedCard;
    /**
     * Card on the left side in hand
     */
    public ACard CurrentLeftCard;
    /**
     * Card on the right side in hand
     */
    public ACard CurrentRightCard;
    /**
     * Card picked to play by current player
     */
    public ACard PickedCard;
    /**
     * Card not picked by current player
     */
    public ACard OtherCard;
    /**
     * Expected next action from player
     */
    public Action NextAction;
    /**
     * A way to communicate to the outside of the game
     */
    public String Message;
    /**
     * Used for validating potential names when setting up game
     */
    public String PotentialName;

    /**
     * Create the game (model) part of the program and set it to its starting state.
     */
    public Model_Game() {
        _deck = new DeckOfCards(this);
        NextAction = Action.start;
        Message = "How many fools in love are trying to win Annette's heart?";
    }

    /**
     * Checks if round/game is over. Awards winner and starts new round if game isn't over yet.
     */
    public void isRoundOverRoutine() {
        if (roundIsOver()) {
            WinnerLastRound = findWinner();
            WinnerLastRound.Hearts += 1;

            if (WinnerLastRound.hasWonTheGame(NumberOfPlayer)) {
                Message = "Congratulations " + WinnerLastRound.Name + " you won the game!.";
                NextAction = Action.gameEnded;
            } else {
                NextAction = Action.newRound;
                Message = "Congratulations " + WinnerLastRound.Name + " you won the round!.";

                while(Players.get(0) != WinnerLastRound) {
                    Player tmp = Players.get(0);
                    Players.remove(0);
                    Players.add(tmp);
                    CurrentPlayer = Players.get(0);
                    CurrentPlayer.IsProtected = false;
                }
            }
        }
    }

    /**
     * Prepares the game for the next player to start their turn.
     */
    public void nextPlayerTurn() {
        Player currentPlayerTmp = CurrentPlayer;
        Message = "";
        RevealedCard = null;
        PickedCard = null;
        OtherCard = null;
        CurrentLeftCard = null;
        CurrentRightCard = null;
        SelectedPlayer = null;
        GuessValue = 0;
        Players.remove(0);
        Players.add(currentPlayerTmp);

        while (!Players.get(0).InPlay) {
            if (Players.get(0) == currentPlayerTmp)
                break;
            Player tmp = Players.get(0);
            Players.remove(0);
            Players.add(tmp);
        }

        CurrentPlayer = Players.get(0);
        CurrentPlayer.IsProtected = false;
        NextAction = Action.drawCard;
    }

    /**
     * Checks if a given command is valid in current game situation
     * @param input Any command
     * @return true if command is valid, false otherwise
     */
    public boolean isValidCommand(Command input) {
        boolean result = false;

        switch (NextAction) {
            case pickCard -> result = input == Command.pickCard_left
                || input == Command.pickCard_right
                || input == Command.showScore;
            case drawCard, newRound -> result = input == Command.drawCard;
            case pickUnprotectedPlayerInPlay -> result = validateCommand_pickUnprotectedPlayerInPlay(input)
                || input == Command.showScore;
            case pickOtherUnprotectedPlayerInPlay -> result = validateCommand_pickOtherUnprotectedPlayerInPlay(input)
                || input == Command.showScore;
            case pickValueNot1 -> result = validateCommand_pickValueNot1(input)
                || input == Command.showScore;
            case endTurn -> result = input == Command.endTurn || input == Command.showScore;
            case gameEnded -> result = input == Command.newGame;
            case enterPlayerNumber -> result = validateCommand_enterPlayerNumber(input);
            case enterPlayerName -> result = input == Command.enterPlayerName;
            case start -> result = input == Command.start;
        }

        if (input == Command.exit)
            result = true;

        if (input == Command.help)
            result = true;

        return result;
    }

    /**
     * Checks if a command is valid to pick an unprotected player in play.
     * @param input Any command
     * @return true if command is used to pick an unprotected player in play, false otherwise
     */
    private boolean validateCommand_pickUnprotectedPlayerInPlay(Command input) {
        if (validateCommand_pickOtherUnprotectedPlayerInPlay(input))
            return true;

        return input == Command.pickPlayer_0;
    }

    /**
     * Checks if a command is valid to pick an unprotected player in play who is not the current player.
     * @param input Any command
     * @return true if command is used to pick an unprotected non-current player in play, false otherwise
     */
    private boolean validateCommand_pickOtherUnprotectedPlayerInPlay(Command input) {
        ArrayList<Command> correctCommands = new ArrayList<>();

        if (Players.get(1).InPlay && !Players.get(1).IsProtected)
            correctCommands.add(Command.pickPlayer_1);

        if (NumberOfPlayer > 2 && Players.get(2).InPlay && !Players.get(2).IsProtected)
            correctCommands.add(Command.pickPlayer_2);

        if (NumberOfPlayer > 3 && Players.get(3).InPlay && !Players.get(3).IsProtected)
            correctCommands.add(Command.pickPlayer_3);

        return correctCommands.contains(input);
    }

    /**
     * Checks if a command is used to pick the value of a card
     * @param input Any command
     * @return true if command is used to pick a value of a card, false otherwise
     */
    private boolean validateCommand_pickValueNot1(Command input)
    {
        ArrayList<Command> correctCommands = new ArrayList<>();
        correctCommands.add(Command.pickValue_2);
        correctCommands.add(Command.pickValue_3);
        correctCommands.add(Command.pickValue_4);
        correctCommands.add(Command.pickValue_5);
        correctCommands.add(Command.pickValue_6);
        correctCommands.add(Command.pickValue_7);
        correctCommands.add(Command.pickValue_8);

        return correctCommands.contains(input);
    }

    /**
     * Checks if a command is for entering a player number.
     * @param input Any command
     * @return true if command is used to enter number of players, false otherwise
     */
    private boolean validateCommand_enterPlayerNumber(Command input) {
        ArrayList<Command> correctCommands = new ArrayList<>();
        correctCommands.add(Command.enterPlayerNumber_2);
        correctCommands.add(Command.enterPlayerNumber_3);
        correctCommands.add(Command.enterPlayerNumber_4);

        return correctCommands.contains(input);
    }

    /**
     * Executes a given command. Always check for validity of command before using this method.
     * @param input LoveLetter.Command to execute
     */
    public void doNextAction(Command input) {
        // will shuffle deck and reset everything to the beginning of a new round
        if (NextAction == Action.newRound && input == Command.drawCard) {
            newRound();
        }

        switch (input) {
            case drawCard -> drawCard();
            case pickCard_left, pickCard_right -> pickCard(input);
            case pickPlayer_0, pickPlayer_1, pickPlayer_2, pickPlayer_3 -> pickPlayer(input);
            case pickValue_2, pickValue_3, pickValue_4, pickValue_5, pickValue_6, pickValue_7, pickValue_8
                    -> pickValue(input);
            case endTurn -> endTurn();
            case newGame -> newGame();
            case enterPlayerNumber_2, enterPlayerNumber_3, enterPlayerNumber_4 -> enterPlayerNumber(input);
            case enterPlayerName -> addPlayer();
            case start -> start();
        }
    }

    /**
     * Starts the game, by entering the setup.
     */
    private void start() { NextAction = Action.enterPlayerNumber; }

    /**
     * Draw a card for current player. Make picking a card the next step.
     */
    private void drawCard() {
        Message = "";
        ACard drawnCard = _deck.drawCard();
        drawnCard.Holder = CurrentPlayer;
        CurrentPlayer.Cards.add(drawnCard);
        CurrentLeftCard = CurrentPlayer.Cards.get(0);
        CurrentRightCard = CurrentPlayer.Cards.get(1);
        NextAction = Action.pickCard;
    }

    /**
     * Picks a card of the current player given to a command. Calls on PickedCard for next step.
     * @param card LoveLetter.Command selecting left or right card
     */
    private void pickCard(Command card) {
        switch (card) {
            case pickCard_left -> {
                PickedCard = CurrentLeftCard;
                OtherCard = CurrentRightCard;
            }
            case pickCard_right -> {
                PickedCard = CurrentRightCard;
                OtherCard = CurrentLeftCard;
            }
        }

        PickedCard.play();
    }

    /**
     * Picks a player given to a command. Calls on PickedCard for next step.
     * @param player LoveLetter.Command selecting player
     */
    private void pickPlayer(Command player) {
        switch(player) {
            case pickPlayer_0 -> SelectedPlayer = Players.get(0);
            case pickPlayer_1 -> SelectedPlayer = Players.get(1);
            case pickPlayer_2 -> SelectedPlayer = Players.get(2);
            case pickPlayer_3 -> SelectedPlayer = Players.get(3);
        }

        PickedCard.play();
    }

    /**
     * Sets guessed value of card given to command. Calls on PickedCard for next step.
     * @param value Value of guessed card
     */
    private void pickValue(Command value) {
        switch (value) {
            case pickValue_2 -> GuessValue = 2;
            case pickValue_3 -> GuessValue = 3;
            case pickValue_4 -> GuessValue = 4;
            case pickValue_5 -> GuessValue = 5;
            case pickValue_6 -> GuessValue = 6;
            case pickValue_7 -> GuessValue = 7;
            case pickValue_8 -> GuessValue = 8;
        }

        PickedCard.play();
    }

    /**
     * Ends turn for current player;
     */
    private void endTurn() {
        PickedCard.play();
    }

    /**
     * Sets player number according to a given command
     * @param number LoveLetter.Command for setting player number
     */
    private void enterPlayerNumber(Command number) {
        switch (number) {
            case enterPlayerNumber_2 -> NumberOfPlayer = 2;
            case enterPlayerNumber_3 -> NumberOfPlayer = 3;
            case enterPlayerNumber_4 -> NumberOfPlayer = 4;
        }

        Players = new ArrayList<>(NumberOfPlayer);
        NextAction = Action.enterPlayerName;
        Message = "What name will mark your letter?";
    }

    /**
     * Adds a player named with PotentialName to the game. Starts the game when all player names have been set.
     */
    private void addPlayer() {
        Players.add(new Player(PotentialName));

        if (Players.size() == NumberOfPlayer) {
            NextAction = Action.newRound;
            newGame();
        }
    }

    /**
     * Deals card to each player and sets up table according to player number.
     */
    private void dealCards() {
        _deck.shuffleCards();
        SideCard = _deck.drawCard();

        if (NumberOfPlayer == 2) {
            OpenCardsFor2Players.add(_deck.drawCard());
            OpenCardsFor2Players.add(_deck.drawCard());
            OpenCardsFor2Players.add(_deck.drawCard());
        }

        for (Player player : Players){
            ACard card = _deck.drawCard();
            player.Cards.add(card);
            card.Holder = player;
        }
    }

    /**
     * Starts a new round of LoveLetters. (One game has many rounds)
     */
    private void newRound() {
        if (NumberOfPlayer == 2) {
            OpenCardsFor2Players = new ArrayList<>(3);
        }

        CurrentPlayer = Players.get(0);

        for (Player player : Players)
            player.newRound();

        dealCards();
        Message = "";
        RevealedCard = null;
        PickedCard = null;
        OtherCard = null;
        CurrentLeftCard = null;
        CurrentRightCard = null;
        SelectedPlayer = null;
        GuessValue = 0;
    }

    /**
     * Starts a new game of LoveLetters.
     */
    private void newGame() {
        for (Player player : Players)
            player.newGame();

        WinnerLastRound = null;
        newRound();
        NextAction = Action.drawCard;
    }

    /**
     * Checks if round of game is over
     * @return true if round is over, false otherwise
     */
    private boolean roundIsOver() {
        if (!_deck.hasCards())
            return true;

        return  GetNumberOfPlayerInPlay() <= 1;
    }

    /**
     * Get winner of a round. Checks for last player in round, high card, high value, last to make a move. Check for
     * end of round before calling this method using roundIsOver(). No safeguards if round is not over!
     * @return LoveLetter.Player that won.
     */
    private Player findWinner() {
        ArrayList<Player> potentialWinners = getPlayersInPlay();

        // last player standing
        if (potentialWinners.size() == 1)
            return potentialWinners.get(0);

        ArrayList<Player> potentialWinners2 = new ArrayList<>(potentialWinners);

        // higher card
        for (Player player : potentialWinners ) {
            for (Player player2: potentialWinners) {
                if (player.Cards.get(0).getValue() > player2.Cards.get(0).getValue())
                    potentialWinners2.remove(player2);
            }
        }

        if (potentialWinners2.size() == 1)
            return potentialWinners2.get(0);

        potentialWinners.clear();

        // value of played cards
        potentialWinners.addAll(potentialWinners2);

        for (Player player : potentialWinners ) {
            for (Player player2: potentialWinners) {
                if (player.getValueOfPlayedCards() > player2.getValueOfPlayedCards())
                    potentialWinners2.remove(player2);
            }
        }

        if (potentialWinners2.size() == 1)
            return potentialWinners2.get(0);

        // instead of asking for age last player of potential winners wins round.
        for(int i = NumberOfPlayer - 1; i >= 0; i--) {
            if (potentialWinners2.contains(Players.get(i)))
                return Players.get(i);
        }

        return potentialWinners.get(0);
    }

    /**
     * Tells the amount of players still in play
     * @return amount of players not out of round
     */
    private int GetNumberOfPlayerInPlay() {
        int inPlay = 0;

        for (Player player: Players) {
            if (player.InPlay)
                inPlay++;
        }

        return inPlay;
    }

    /**
     * Get all players in play
     * @return ArrayList of players
     */
    private ArrayList<Player> getPlayersInPlay() {
        ArrayList<Player> inPlayPlayers = new ArrayList<>();

        for (Player player: Players) {
            if (player.InPlay)
                inPlayPlayers.add(player);
        }

        return inPlayPlayers;
    }

    /**
     * Get all active players (including current player), who are unprotected
     * @return ArrayList of players
     */
    public ArrayList<Player> getPlayersInPlayUnprotected() {
        ArrayList<Player> inPlayNotProtectedPlayers = new ArrayList<>();

        for (Player player: Players) {
            if (player.InPlay && !player.IsProtected)
                inPlayNotProtectedPlayers.add(player);
        }

        return inPlayNotProtectedPlayers;
    }

    /**
     * Get all other players (not current), who are unprotected
     * @return ArrayList of players
     */
    public ArrayList<Player> getOtherPlayersInPlayUnprotected() {
        ArrayList<Player> inPlayNotProtectedPlayers = getPlayersInPlayUnprotected();
        inPlayNotProtectedPlayers.remove(CurrentPlayer);

        return inPlayNotProtectedPlayers;
    }

    /**
     * Get which round is currently being played
     * @return Current round number starting with 0
     */
    public int getRoundNumber() {
        int rounds = 0;

        for (Player player : Players)
            rounds += player.Hearts;

        return rounds;
    }

    /**
     * Tells the amount of cards left in the deck.
     * @return amount of cards left in the deck
     */
    public int getRemainingDeckSize() {
        return _deck.Cards.size();
    }

    /**
     * Checks if a given name is a valid name for a player. Checks length, letters and uniqueness and sets Message.
     * @param name Potential player name
     * @return true if name is valid, false otherwise
     */
    public boolean isValidPlayerName(String name) {
        if (name.length() > 20) {
            Message = "I don't have enough ink. Pick a shorter name please.";

            return false;
        }

        char[] stringArray = name.replace(" ", "").toCharArray();

        for (char c : stringArray) {
            if (!Character.isLetter(c)) {
                Message = "Keep it simple. Letters only.";

                return false;
            }
        }

        for (Player player: Players) {
            if (name.equals(player.Name)) {
                Message = "The princess might mix you up. Unique names only.";

                return false;
            }
        }

        PotentialName = name;
        Message = "Got another name for me?";
        return true;
    }

    /**
     * Checks if a given int is valid number of players. Sets Message if false.
     * @param numberOfPlayer int representing player number
     * @return true is between 2 and 4
     */
    public boolean isValidNumberOfPlayers(int numberOfPlayer) {
        boolean result = 2 <= numberOfPlayer && numberOfPlayer <= 4;

        if (!result)
            Message = "A princess doesn't read all day. 2 to 4 suitors at a time.";

        return result;
    }

    /**
     * Get a hashtable mapping player names to the command for picking them
     * @return Hashtable of players in game and not protected
     */
    public Hashtable<String, Command> getCommandForPickPlayerPerNameHashtable() {
        Hashtable<String,Command> nameTable = new Hashtable<>();

        for(int i = 0; i < NumberOfPlayer; i++) {
            Player player = Players.get(i);

            if (player.InPlay && !player.IsProtected){
                switch (i) {
                    case 0 -> nameTable.put(player.Name, Command.pickPlayer_0);
                    case 1 -> nameTable.put(player.Name, Command.pickPlayer_1);
                    case 2 -> nameTable.put(player.Name, Command.pickPlayer_2);
                    case 3 -> nameTable.put(player.Name, Command.pickPlayer_3);
                }
            }
        }

        return nameTable;
    }
}

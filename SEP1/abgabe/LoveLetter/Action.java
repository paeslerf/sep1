package LoveLetter;

/**
 * Used to determine what next to do and what to display
 * @hidden
 */
public enum Action {
    start,
    help,
    enterPlayerNumber,
    enterPlayerName,
    newRound, // used as special case of draw card, where table has to be reset
    drawCard, // showHand and startTurn will use this
    pickCard,
    pickOtherUnprotectedPlayerInPlay,
    pickUnprotectedPlayerInPlay,
    pickValueNot1,
    endTurn,
    gameEnded
}

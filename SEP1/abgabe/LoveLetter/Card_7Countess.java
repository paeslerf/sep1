package LoveLetter;

/**
 * Countess
 * @hidden
 */
public class Card_7Countess extends ACard {
    private static final String _name = "Wilhelmina";
    private static final String _role = "Countess";
    private static final int _value = 7;
    private static final String _effect = "Has to be played when other card is King or Prince!";
    public Card_7Countess(DeckOfCards deck)
    {
        Deck = deck;
    }

    @Override
    public String getName()
    {
        return _name;
    }

    @Override
    public String getRole() { return _role; }

    @Override
    public int getValue()
    {
        return _value;
    }

    @Override
    public String getEffect() {return _effect;}

    @Override
    public void play()
    {
        switch (getNextAction()) {
            case pickCard -> {
                discard();

                if (Deck.Game.OtherCard instanceof Card_6King)
                    Deck.Game.Message = "You had to pick the Countess for the King.";
                else if (Deck.Game.OtherCard instanceof Card_5Prince)
                    Deck.Game.Message = "You had to pick the Countess for the Prince.";
                else
                    Deck.Game.Message = "You're playing mind games with the others.";

                Deck.Game.NextAction = Action.endTurn;
                Deck.Game.isRoundOverRoutine();
            }
            case endTurn -> Deck.Game.nextPlayerTurn();
        }
    }
}

package LoveLetter;

import java.util.ArrayList;

/**
 * Prince
 * @hidden
 */
public class Card_5Prince extends ACard {
    private static final String _name = "Arnaud";
    private static final String _role = "Prince";
    private static final int _value = 5;
    private static final String _effect = "Pick a player who has to discard and draw.";

    public Card_5Prince(DeckOfCards deck) { Deck = deck; }

    @Override
    public String getName()
    {
        return _name;
    }

    @Override
    public String getRole() { return _role; }

    @Override
    public int getValue()
    {
        return _value;
    }

    @Override
    public String getEffect() {return _effect;}

    @Override
    public void play()
    {
        switch (getNextAction()) {
            case pickCard -> {
                if (Deck.Game.OtherCard instanceof Card_7Countess) {
                    ACard tmp = Deck.Game.PickedCard;
                    Deck.Game.PickedCard = Deck.Game.OtherCard;
                    Deck.Game.OtherCard = tmp;
                    Deck.Game.PickedCard.play();
                    break;
                }

                ArrayList<Player> potentialTargets = Deck.Game.getPlayersInPlayUnprotected();

                if (potentialTargets.size() == 1) {
                    Deck.Game.SelectedPlayer = potentialTargets.get(0);
                    discard();

                    if (!(Deck.Game.OtherCard instanceof Card_8Princess)) {
                        ACard card;

                        if (Deck.hasCards())
                            card = Deck.drawCard();
                        else {
                            card = Deck.Game.SideCard;
                            Deck.Game.SideCard = null;
                        }

                        card.Holder = Deck.Game.CurrentPlayer;
                        Deck.Game.CurrentPlayer.Cards.add(card);
                        Deck.Game.OtherCard.discard();
                        Deck.Game.CurrentLeftCard = card;
                        Deck.Game.CurrentRightCard = null;
                        Deck.Game.Message = "You had to pick yourself and draw a new card.";
                    } else {
                        Deck.Game.OtherCard.discard();
                        Deck.Game.CurrentLeftCard = null;
                        Deck.Game.CurrentRightCard = null;
                        Deck.Game.Message = "You had to pick yourself and had the Princess. You're out.";
                    }

                    Deck.Game.NextAction = Action.endTurn;
                    Deck.Game.isRoundOverRoutine();
                } else {
                    Deck.Game.NextAction = Action.pickUnprotectedPlayerInPlay;
                    Deck.Game.Message = "Who should discard?";
                }
            }
            case pickUnprotectedPlayerInPlay -> {
                discard();
                ACard otherPlayersCard = Deck.Game.SelectedPlayer.Cards.get(0);

                if (!(otherPlayersCard instanceof Card_8Princess)) {
                    ACard card;

                    if (Deck.hasCards())
                        card = Deck.drawCard();
                    else
                        card = Deck.Game.SideCard;

                    card.Holder = Deck.Game.SelectedPlayer;
                    Deck.Game.SelectedPlayer.Cards.add(card);
                    Deck.Game.Message = Deck.Game.SelectedPlayer.Name + " discarded and drew a new card.";
                } else {
                    Deck.Game.Message = Deck.Game.SelectedPlayer.Name + " had to discard the princess. They are out.";
                }

                if (Deck.Game.SelectedPlayer == Deck.Game.CurrentPlayer) {
                    otherPlayersCard = Deck.Game.OtherCard;
                    Deck.Game.CurrentLeftCard = null;
                    Deck.Game.CurrentRightCard = null;
                }

                otherPlayersCard.discard();

                Deck.Game.NextAction = Action.endTurn;
                Deck.Game.isRoundOverRoutine();
            }
            case endTurn -> Deck.Game.nextPlayerTurn();
        }
    }
}

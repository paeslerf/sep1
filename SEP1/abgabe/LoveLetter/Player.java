package LoveLetter;

import java.util.LinkedList;

/**
 * Participant in the game for the Heart of Princess Annette
 */
public class Player {
    /**
     * Name of Player
     */
    public String Name;
    /**
     * Number of hearts a player has earned
     */
    public int Hearts;
    /**
     * Status of player is still in play for the heart of the princess this round
     */
    public boolean InPlay;
    /**
     * Status of player determining if they can be picked by others
     */
    public boolean IsProtected;
    /**
     * Cards the player has in their hand
     */
    public LinkedList<ACard> Cards = new LinkedList<>();
    /**
     * Cards the player discarded
     */
    public LinkedList<ACard> PlayedCards = new LinkedList<>();

    // if player had to trade cards, there is a way to know it
    /**
     * Card gotten if player traded cards with another player
     */
    public ACard CardGotten;
    /**
     * Card given away if player traded cards with another player
     */
    public ACard CardGivenAway;
    /**
     * Player who traded a card with this player
     */
    public Player CardSwitchingPartner;

    /**
     * Creates a player with a name and put them into status of a beginning round
     * @param name Name for the player
     */
    public Player(String name) {
        Name = name;
        Hearts = 0;
        InPlay = true;
        IsProtected = false;
        CardGotten = null;
        CardGivenAway = null;
        CardSwitchingPartner = null;
    }

    /**
     * Resets everything to the start conditions of a new round
     */
    public void newRound() {
        InPlay = true;
        IsProtected = false;
        Cards.clear();
        PlayedCards.clear();
        CardGotten = null;
        CardGivenAway = null;
        CardSwitchingPartner = null;
    }

    /**
     * Resets everything to the start conditions of a new round and resets Hearts to 0.
     */
    public void newGame() {
        newRound();
        Hearts = 0;
    }

    /**
     * Adds up all the values of played cards
     * @return Sum of values. 0 for no cards played.
     */
    public int getValueOfPlayedCards() {
        int value = 0;

        for (ACard card : Cards )
            value += card.getValue();

        return value;
    }

    /**
     * Checks whether a player has won the game
     * @param numberOfPlayers Number of players taking part in game
     * @return true if player has won, false otherwise
     */
    public boolean hasWonTheGame(int numberOfPlayers) {
        if(numberOfPlayers == 2)
            return  Hearts >= 7;

        if(numberOfPlayers == 3)
            return  Hearts >= 5;

        if(numberOfPlayers == 4)
            return  Hearts >= 4;

        return false;
    }
}
